terraform {
  backend "local" { path = "tfstate/terraform.tfstate" }
}
provider "google" {
  credentials = file("../creds/gcloud_creds.json")
  project = var.project_id
  region  = var.region
}
resource "google_project_service" "sqladmin" {
  service            = "sqladmin.googleapis.com"
  disable_on_destroy = false
}
resource "google_sql_database_instance" "my_instance" {
  name    = "connasse666"
  database_version = "POSTGRES_11"
  depends_on       = [google_project_service.sqladmin]
  settings {
    tier = "db-f1-micro"
    maintenance_window {
      day  = "7" # sunday
      hour = "1"
    }
    backup_configuration {
      enabled    = true
      start_time = "01:00"
    }
  }
}
resource "google_sql_database" "my_db" {
  name     = "connasse666"
  instance = google_sql_database_instance.my_instance.name
}
resource "google_sql_user" "todo" {
  name     = "todo"
  instance = google_sql_database_instance.my_instance.name
  password = var.db_password
}
output "instance_name" {
  value = google_sql_database_instance.my_instance.connection_name
}