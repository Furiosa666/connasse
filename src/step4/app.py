# pylint: disable=C0326
# C0326 = bad-whitespace (around keyword arg. assignment, brackets, etc.)
from flask import Flask
from flask_restplus import Api, Resource
import model
import bizlogic

flask_app = Flask(__name__)
api = Api(
    app         = flask_app,
    version     = "0.1",
    title       = "Todo list",
    description = "Some stuff you need to do."
)

# Whenever APIs are defined under a given namespace,
# they appear under a given heading in Swagger
ns = api.namespace('Todo', description='Todo operations')

import sql

# Import the models
# api.model(name=None, model=None, mask=None, **kwargs)
todo_create_req_model  = api.model(**model.TodoCreateReq)
# api.schema_model(name=None, schema=None)
todo_create_resp_model = api.schema_model(**model.TodoCreateResp)
todo_list_resp_model   = api.schema_model(**model.TodoListResp)
todo_get_resp_model    = api.schema_model(**model.TodoGetResp)

dao = bizlogic.TodoDAO(sql.cnxpool)

@ns.route("/")
class TodoList(Resource):
    '''Shows a list of all todos, and lets you POST to add new tasks'''

    @ns.response(200, "OK", todo_list_resp_model)
    def get(self):
        '''List all tasks'''
        return dao.todos()

    @ns.expect(todo_create_req_model, validate=True)  # Validate the input
    # This should work but there is a bug :
    #@ns.marshal_with(todo_create_resp_model, code=201)
    # Cf https://github.com/noirbizarre/flask-restplus/issues/293
    # But this is working as expected :
    @ns.response(201, "Created",
        todo_create_resp_model)  # Filter the output + API doc
    def post(self):
        '''Create a new task'''
        return dao.create(api.payload), 201

@ns.route('/<int:id>')
@ns.response(200, 'Found')
@ns.response(404, 'Not found')
@ns.param('id', 'The task identifier')
class Todo(Resource):
    '''Show a single todo item and lets you delete them'''

    @ns.response(200, "Found",
        todo_get_resp_model)  # Filter the output + API doc
    def get(self, id):
        '''Fetch a given resource'''
        record = dao.get(id)
        if record:
            return record
        api.abort(404, "Todo {} doesn't exist".format(id))

    @ns.response(204, 'Deleted')
    def delete(self, id):
        '''Delete a task given its identifier'''
        todo = dao.delete(id)
        if todo:
            return '', 204
        api.abort(404, "Todo {} doesn't exist".format(id))

    @ns.expect(todo_create_req_model, validate=True)  # Validate the input
    def put(self, id):
        '''Update a task given its identifier'''
        return dao.update(id, api.payload)


if __name__ == '__main__':
    flask_app.run(debug=True)
