# A data access object (DAO) is an object that provides an abstract interface
# to some type of database or other persistence mechanism

class TodoDAO(object):
    def __init__(self, cnxpool):
        self.cnx = cnxpool.getconn()

    def todos(self):
        query = "SELECT id, title, description FROM todo;"
        with self.cnx.cursor() as cursor:
            cursor.execute(query)
            data = cursor.fetchall()
        self.cnx.commit()
        results = []
        if not data:
            return results
        for row in data:
            results.append({'id': row[0], 'title': row[1], 'description': row[2]})
        return results

    def get(self, id):
        query =  "SELECT id, title, description"
        query += " FROM todo WHERE ID={};".format(id)
        with self.cnx.cursor() as cursor:
            cursor.execute(query)
            data = cursor.fetchone()
        self.cnx.commit()
        if not data:
            return None
        return { 'id': data[0], 'title': data[1], 'description': data[2] }

    def create(self, data):
        todo = data
        insert = "INSERT INTO todo(title, description)"
        insert += " VALUES ('{}', '{}');".format(
            data['title'], data['description'])
        with self.cnx.cursor() as cursor:
            cursor.execute(insert)
        self.cnx.commit()
        return todo

    def update(self, id, data):
        todo = self.get(id)
#        if todo :
#            update = "UPDATE sql"
#            with self.cnx.cursor() as cursor:
#                cursor.execute(update)
#            self.cnx.commit()
        return todo

    def delete(self, id):
        todo = self.get(id)
        if todo :
            delete = "DELETE FROM todo WHERE id={}".format(id)
            with self.cnx.cursor() as cursor:
                cursor.execute(delete)
            self.cnx.commit()
        return todo
