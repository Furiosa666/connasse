import sqlalchemy
import psycopg2.pool
import credentials as creds

#engine = sqlalchemy.create_engine(
#    sqlalchemy.engine.url.URL(
#        drivername='postgres+pg8000',
#        username=creds.DB_USER,
#        password=creds.DB_PASS,
#        database=creds.DB_NAME,
#        query={
#            'unix_sock': '/cloudsql/{}/.s.PGSQL.5432'.format(
#                creds.CLOUD_SQL_CONNECTION_NAME)
#        }
#    ),
#)

createtable = """
    CREATE TABLE IF NOT EXISTS todo (
        id SERIAL NOT NULL,
        title VARCHAR(30) NOT NULL,
        description VARCHAR(100) NOT NULL,
        PRIMARY KEY (id)
    );
"""

# Create tables (if they don't already exist)
#with engine.connect() as conn:
#    conn.execute(createtable)
#    conn.close()

db_config = {
    'user': creds.DB_USER,
    'password': creds.DB_PASS,
    'database': creds.DB_NAME,
    'host': '/cloudsql/{}'.format(creds.CLOUD_SQL_CONNECTION_NAME)
}

cnxpool = psycopg2.pool.ThreadedConnectionPool(
    minconn=1, maxconn=3, **db_config)

cnx = cnxpool.getconn()
with cnx.cursor() as cursor:
    cursor.execute(createtable)
cnx.commit()
if cnx is not None:
    cnx.close()

