# pylint: disable=C0326

from flask_restplus import fields

# You can have more complex and nested structures
# or define your own custom field types and models.
# Raw provides a base field class from which others should extend.
# You have many standard field types implementing
# many Swagger or JSON schema validators.
# Cf https://swagger.io/docs/specification/data-models/keywords/

TodoCreateReq = {
    'name': 'TodoCreateReq',
    'model': {
        'title': fields.String(
            required = True,
            description = 'Title of the task',
            example = 'My task',
            # Just to show a regex validation pattern
            pattern = '^[A-Z].*$',
            help = 'The title cannot be blank and start with an uppercase char.',
            ),
        'description': fields.String(
            required = True,
            description = 'What to do',
            example = 'Whatever.',
            help = 'The description cannot be blank.',
            ),
    }
}

# You can also define models using JSON Schema directly, cf
# https://flask-restplus.readthedocs.io/en/stable/marshalling.html#define-model-using-json-schema
# Let's do this.

TodoCreateResp = {
    'name': 'TodoCreateResp',
    'schema': {
        'type': 'object',
        'required' : [ 'id' ],
        'properties': {
            'id':    { 'type': 'integer' },
            'title': { 'type': 'string'  }
        }
    }
}

TodoListResp = {
    'name': 'TodoListResp',
    'schema': {
        'type': 'array',
        'items': {
            'type': 'object',
            'properties': {
                'id':          { 'type': 'integer' },
                'title':       { 'type': 'string'  },
                'description': { 'type': 'string' }
            },
            'required': [ 'id', 'title' ]
        }
    }
}

TodoGetResp = {
    'name': 'TodoGetResp',
    'schema': {
        'type': 'object',
        'required' : [ 'id' ],
        'properties': {
            'id':          { 'type': 'integer' },
            'title':       { 'type': 'string'  },
            'description': { 'type': 'string'  }
        }
    }
}

