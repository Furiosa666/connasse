from flask import Flask
from flask_restplus import Api

class Server(object):
    def __init__(self):
        self.flask_app = Flask(__name__)
        self.api = Api(self.flask_app,
            version     = "0.1",
            title       = "Todo list",
            description = "Some stuff you need to do."
        )

    def run(self):
        self.flask_app.run(debug=True)

server = Server()
