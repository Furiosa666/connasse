# A data access object (DAO) is an object that provides an abstract interface
# to some type of database or other persistence mechanism

class TodoDAO(object):
    def __init__(self, cnxpool):
        self.cnx = cnxpool.getconn()

    def todos(self):
        query = "SELECT id, title, description FROM todo;"
        with self.cnx.cursor() as cursor:
            cursor.execute(query)
            data = cursor.fetchall()
        results = []
        if not data:
            return results
        for row in data:
            results.append({'id': row[0], 'title': row[1], 'description': row[2]})
        return results

    def get(self, id):
        query =  "SELECT id, title, description FROM todo WHERE ID=%s;"
        with self.cnx.cursor() as cursor:
            cursor.execute(query, (id,))
            data = cursor.fetchone()
        if not data:
            return None
        return { 'id': data[0], 'title': data[1], 'description': data[2] }

    def create(self, data):
        todo = data
        insert = "INSERT INTO todo(title, description) VALUES (%s, %s);"
        with self.cnx.cursor() as cursor:
            cursor.execute(insert, (data['title'], data['description'],))
        self.cnx.commit()
        return todo

    def update(self, id, data):
        todo = self.get(id)
        print(todo)
        # update to implement here
        if todo :
            update ="UPDATE todo SET title = %s,description = %s WHERE id=%s;"
            with self.cnx.cursor() as cursor:
                cursor.execute(update,(data['title'],data['description'],id,))
            self.cnx.commit()
        todo = self.get(id)
        return todo

    def delete(self, id):
        todo = self.get(id)
        if todo :
            delete = "DELETE FROM todo WHERE id=%s;"
            with self.cnx.cursor() as cursor:
                cursor.execute(delete, (id,))
            self.cnx.commit()
        return todo
