SHELL=/bin/bash

venv:
	@python3 -m venv venv

install: venv
	#requirements here
	@source venv/bin/activate && pip install -r requirements.txt && deactivate

localrun: install
	#run the app
	#@source venv/bin/activate && python3 main.py
	#@source venv/bin/activate && python3 app.py
	@source venv/bin/activate && gunicorn src.step5b.app:flask_app --pythonpath src/step5b && deactivate

test:
	export PYTHONPATH=./src/step5b && source venv/bin/activate && python -m pytest --cov=src/step5b -s && deactivate

deploy:
	gcloud app deploy

.PHONY: help test