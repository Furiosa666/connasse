from mock import patch
import re
import sys
sys.modules['sql'] = __import__('fakesql')
import resources
import json
from mock import patch, MagicMock

def test_list(client):
    with patch('bizlogic.TodoDAO.todos') as mocked_todos:
        mocked_todos.return_value = [
            { 'id': 1, 'title': 'Connasse', 'description': 'Connassitude' } ,
            { 'id': 2, 'title': 'An other task', 'description': 'bar' },
        ]
        response = client.get("/Todo/")
        assert response.is_json
        assert response.json[1]['description'] == 'bar'
        assert response.status_code == 200

def test_delete_todo(client):
    todo = {
        'id': 1,
        'title': 'Connasse',
        'description': 'Connassitude'
    }
    with patch('bizlogic.TodoDAO.get') as mocked_get:
        mocked_get.return_value = todo
        with patch('bizlogic.TodoDAO.delete') as mocked_delete:
            mocked_delete.return_value = todo
            response = client.delete("/Todo/1")
            assert response.status_code == 204
		
def test_get_todo_not_found(client):
    with patch('bizlogic.TodoDAO.get') as mocked_todos:
        mocked_todos.return_value = None
        response = client.get("/Todo/1")
        assert response.is_json
        assert re.search("doesn't exist", response.json['message'])
        assert response.status_code == 404

def test_get_todo_found(client):
    with patch('bizlogic.TodoDAO.get') as mocked_todos:
        mocked_todos.return_value = {
            'id': 1, 'title': 'Connasse', 'description': 'Connassitude'
        }
        response = client.get("/Todo/1")
        assert response.json['id'] == 1
        assert response.status_code == 200

def test_post_todo(client):
    todo = {
    'id': 1,
    'title': 'Connasse',
    'description': 'Connassitude'
    }
    with patch('bizlogic.TodoDAO.create') as mocked_create:
        mocked_create.return_value = todo
        headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        }
        data = { 'title': 'Test', 'description': 'Whatever' }
        response = client.post("/Todo/", data = json.dumps(data), headers = headers) 
        assert response.status_code == 201

def test_post_todo_invalid(client):
	todo = {
    'id': 1,
    'title': 'Connasse',
    'description': 'Connassitude'
    }
	with patch('bizlogic.TodoDAO.create') as mocked_create:
		mocked_create.return_value = todo
		headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
		}
		data = { 'title': 'test', 'description': 'Whatever' }
		response = client.post("/Todo/", data = json.dumps(data), headers = headers) 
		assert re.search("does not match", response.json['errors']['title'])
		assert response.status_code == 400

def test_update_todo_invalid(client):
	todo = {
	'id': 1,
	'title': 'Connasse',
	'description': 'Connassitude'
	}
	with patch('bizlogic.TodoDAO.get') as mocked_get:
		mocked_get.return_value = todo
		with patch('bizlogic.TodoDAO.update') as mocked_update:
			mocked_update.return_value = todo
			headers = {
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			}
			data = { 'title': 'test', 'description': 'Whatever' }
			response = client.post("/Todo/", data = json.dumps(data), headers = headers)
			assert re.search("does not match", response.json['errors']['title'])
			assert response.status_code == 400
