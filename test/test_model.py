import model
import sys
from server import server

sys.modules['sql'] = __import__('fakesql')

api = server.api

def test_model_todo_create():
    todo_create_req_model  = api.model(**model.TodoCreateReq)
    assert 'title' in todo_create_req_model.keys()
